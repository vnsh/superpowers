
const fs = require('fs'),
    path = require('path'),
    http = require('http');
const env = process.env.NODE_ENV || 'development';
let config = require(path.join(__dirname, './', 'config.js'))[env];



const User = db.define('User', {
  username: DataTypes.STRING,
  id : {
    type : DataTypes.INTEGER,
    primaryKey: true
  },
  password : DataTypes.STRING,
  email : DataTypes.STRING
});

const captures = db.define('captures', {
  id : {
    type : DataTypes.INTEGER,
    primaryKey: true
  },
  content : DataTypes.STRING,
  is_public : DataTypes.BOOLEAN
});

const tag = db.define('tag', {
  id : {
    type : DataTypes.INTEGER,
    primaryKey: true
  },
  text : DataTypes.STRING,
})

const user_capture_mapping = db.define('user_capture_mappings', {
  user_id : DataTypes.INTEGER,
  capture_id : DataTypes.INTEGER,
});

const user_manager_mapping = db.define('user_manager_mappings', {
  user_id : DataTypes.INTEGER,
  manager_id : DataTypes.INTEGER,
});

const capture_tag_mapping = db.define('capture_tag_mappings', {
  tagid : DataTypes.INTEGER,
  captureid : DataTypes.INTEGER,
  count : DataTypes.INTEGER
});


