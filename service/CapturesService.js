"use strict";
const knex = require("../knex.js");
var _ = require("lodash");
const { tags } = require("./TagsService.js");

/**
 * Create Captures
 * This is to create captures
 *
 * body Capture
 * no response value expected for this operation
 **/
exports.cCaptures = function (body) {
  let content = JSON.stringify(body.capture_content.trim());
  let isPublic = body.is_public;
  let isCelebrate = body.is_celebrate;
  let source_id = body.source_is;
  let tags = body.tags;
  let captureid;
  let insertData = [];

  return new Promise(function (resolve, reject) {
    let query = knex.transaction(function (t) {
      return (
        knex("captures")
          .transacting(t)
          .insert({ content: content, is_public: isPublic, is_celebrate: isCelebrate })
          .then(function (response) {
            captureid = response;
            return knex("capture_user_mapping").transacting(t).insert({ creator_id: source_id, capture_id: captureid, shared_with_id: source_id });
          })
          //fetch tags id
          .then(function () {
            let res = knex("tags").whereIn("text", tags).pluck("id");
            res.then((result) => {
              result.forEach((element) => {
                insertData.push({
                  capture_id: captureid,
                  tag_id: element,
                });
              });
              return knex("capture_tags_mapping").insert(insertData);
            });
          })
          .then(t.commit)
          .catch(t.rollback)
      );
    });

    query.then(() => {
      resolve({
        message: "Capture created successfully",
      });
    });
  });
};

/**
 * Get captures
 *
 *
 * email String The email/id for user that needs to be fetched.
 * returns Capture
 **/
exports.getAllCaptures = function (currentUserId) {
  let tagList = [];
  //1. Check if currentuserid is id or email, if email, convert to id
  return new Promise(async function (resolve, reject) {
    let publicCaptures = await knex("captures").where("is_public", 1);
    //fetch all the captures that are public; store in a list
    let privateCaptures = await knex("capture_user_mapping")
      .where("shared_with_id", currentUserId)
      .join("captures", "capture_user_mapping.capture_id", "=", "captures.id"); //implement join here
    let captureIds = [];
    //fetch all the captures from capture_user mapping by filtering on column shared with id
    let combinedCaptures = _.merge(publicCaptures, privateCaptures);
    captureIds = combinedCaptures.map((obj) => obj.id);
    let res = knex("capture_tags_mapping").whereIn("id", captureIds);
    res.then((list) => {
      //list object is list of captures - tag mapping
      tagList = list.map((obj) => obj.tag_id);
      //we got list of tag ids
      let res = knex("tags").whereIn("id", tagList);
      //fetch text of tags
      //for each tagName
      res.then((tagNamesList) => {
        list.forEach((element) => {
          //we have now list of capture id's to modify
          let tagId = element.tag_id;
          let tagData = _.find(tagNamesList, { id: tagId }).text;
          combinedCaptures.map((capture) => {
            if (capture.id === element.capture_id) {
              console.log(capture.id);
              capture.tags = [];
              capture.tags.push(tagData);
            }
          });
        });
        let result = _.uniqBy(combinedCaptures, "id");
        resolve(result);
      });
    });
  });
};

/**
 * Share capture with manager
 * This is to share a user capture with managers
 *
 * shared_from String The id for user that is sharing the capture
 * shared_to String The manager id
 * no response value expected for this operation
 **/
exports.shareCaptureWithManager = function (body) {
  return new Promise(function (resolve, reject) {
    let res = knex("capture_user_mappings").insert({ capture_id: body.capture_id, creator_id: body.shared_from, shared_with_id: body.shared_to });
    res.then(() => {
      resolve({
        message: "Capture shared successfully",
      });
    });
  });
};
