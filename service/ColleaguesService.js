"use strict";
const knex = require("../knex.js");

/**
 * Get colleague list
 *
 *
 * id String The email/id for user whose colleagues are needed
 * no response value expected for this operation
 **/
exports.getColleagues = function (userid) {
  return new Promise(function (resolve, reject) {
    let res = 400;
    res = knex("users").select("name", "email", "id");
    res
      .then((rows) => {
        resolve(rows);
      })
      .catch((err) => {
        console.log(err);
        throw err;
      });
  });
};
