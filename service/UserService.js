"use strict";
const { result } = require("lodash");
const knex = require("../knex.js");
var _ = require("lodash");

/**
 * Fetch Data From Tags
 * This is to list all the tags
 *
 * body Tags
 * no response value expected for this operation
 **/
exports.tags = function (body) {
  return new Promise(function (resolve, reject) {
    let res = knex("tags").pluck("text");
    res.then(() => {
      resolve(res);
    });
  });
};

/**
 * Fetch all tags
 * This is to fetch data for all tags for a user
 *
 * id String The email/id for user that needs to be fetched.
 * no response value expected for this operation
 **/
exports.toptagsfetcher = function (currentUserId) {
  return new Promise(async function (resolve, reject) {
    //from user id , get list of all the capture ids
    if (currentUserId != null) {
      let listCaptures = await knex("capture_user_mapping").where("creator_id", currentUserId);
      let captureIds = listCaptures.map((obj) => obj.id);
      let res = knex("capture_tags_mapping").whereIn("capture_id", captureIds);
      res.then((list) => {
        //list object is list of captures - tag mapping
        let tagList = list.map((obj) => obj.tag_id);
        let dataSet = tagList;
        //we got list of tag ids
        let res = knex("tags").whereIn("id", tagList);
        const counts = {};

        for (const num of dataSet) {
          counts[num] = counts[num] ? counts[num] + 1 : 1;
        }

        //fetch text of tags
        //for each tagName
        const responseData = {};
        res.then((tagNamesList) => {
          //const totalKeys = Object.keys(counts);
          // for (const key of totalKeys) {
          //   for (const tag of tagNamesList) {
          //     if (tag.id == key) {
          //       responseData[tag.text] = counts[key];
          //     }
          //   }
          // }

          resolve(tagNamesList);
        });
      });
    }
  });
};

/**
 * Create user
 * This is to add a user document
 *
 * body User Create user object
 * no response value expected for this operation
 **/
exports.createUser = function (body) {
  return new Promise(function (resolve, reject) {
    let res = knex("users").insert({ email: body.email, name: body.name, password: body.password });
    res.then(() => {
      resolve({
        message: "User created successfully",
      });
    });
  });
};

/**
 * Delete user
 * This can delete a user from DB
 *
 * email String The email for the user that needs to be deleted
 * no response value expected for this operation
 **/
exports.deleteUser = function (email) {
  return new Promise(function (resolve, reject) {
    resolve();
  });
};

/**
 * Get user by user email / user id
 *
 *
 * id String The email/id for user that needs to be fetched.
 * returns User
 **/
exports.getUser = function (id) {
  return new Promise(function (resolve, reject) {
    let res = 400;
    let isNum = /^-?\d+$/.test(id);
    if (isNum) {
      res = knex("users").where({ id: id }).select("name", "email", "id");
    } else {
      id = id.toLowerCase();
      res = knex("users").where({ email: id }).select("name", "email", "id");
    }
    res
      .then((rows) => {
        resolve(rows);
      })
      .catch((err) => {
        console.log(err);
        throw err;
      });
  });
};
