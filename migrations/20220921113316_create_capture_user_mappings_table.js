/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.up = function (knex) {
  return knex.schema.createTable("capture_user_mapping", function (table) {
    table.increments("id");
    table.integer("creator_id");
    table.integer("capture_id");
    table.integer("shared_with_id");
    table.timestamps(true, true);
  });
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
exports.down = function (knex) {
  return knex.schema.dropTable("capture_user_mapping");
};
