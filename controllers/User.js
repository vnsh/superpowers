"use strict";

var utils = require("../utils/writer.js");
var User = require("../service/UserService");

module.exports.createUser = function createUser(req, res, next) {
  var body = req.swagger.params["body"].value;
  User.createUser(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.deleteUser = function deleteUser(req, res, next) {
  var email = req.swagger.params["email"].value;
  User.deleteUser(email)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getUser = function getUser(req, res, next) {
  var id = req.swagger.params["id"].value;
  User.getUser(id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.toptagsfetcher = function toptagsfetcher(req, res, next) {
  var id = req.swagger.params["id"].value;
  User.toptagsfetcher(id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.tags = function tags(req, res, next) {
  User.tags()
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
