"use strict";

var utils = require("../utils/writer.js");
var Tags = require("../service/TagsService");

module.exports.getRelevanttags = function getRelevanttags(req, res, next) {
  var text = req.swagger.params["text"].value;
  Tags.getRelevanttags(text)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
