'use strict';

var utils = require('../utils/writer.js');
var Colleagues = require('../service/ColleaguesService');

module.exports.getColleagues = function getColleagues (req, res, next) {
  var id = req.swagger.params['id'].value;
  Colleagues.getColleagues(id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
