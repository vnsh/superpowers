"use strict";

var utils = require("../utils/writer.js");
var Captures = require("../service/CapturesService");

module.exports.createCaptures = function createCaptures(req, res, next) {
  var body = req.swagger.params["body"].value;
  Captures.cCaptures(body)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getAllCaptures = function getAllCaptures(req, res, next) {
  var email = req.swagger.params["id"].value;
  Captures.getAllCaptures(email)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.shareCaptureWithManager = function shareCaptureWithManager(req, res, next) {
  var shared_from = req.swagger.params["shared_from"].value;
  var shared_to = req.swagger.params["shared_to"].value;
  Captures.shareCaptureWithManager(shared_from, shared_to)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
