module.exports = {
  env: {
    commonjs: true,
    es2021: true,
    node: true,
    mocha: true,
  },
  extends: [''],
  parserOptions: {
    ecmaVersion: 12,
  },
  rules: {
  },
};
